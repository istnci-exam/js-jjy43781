// ***********************************************
// Conditional Statements
// ***********************************************

//If Statement
let sale = true; //sale 변수선언
if (sale) {
    console.log('Time to buy!');
  }
sale = false;
if (sale) {
    console.log('No buy!');
}

//If...Else Statements
let sale = true;
sale = false;
if(sale) {
  console.log('Time to buy!');
} else {
  console.log('Time to wait for a sale.');
}

//Comparison Operators
let hungerLevel = 7;
if (hungerLevel > 7) {
    console.log('Time to eat!');
  } else {
    console.log('We can eat later!');
  }

//Logical Operators
let mood = 'sleepy';
let tirednessLevel = 6;
if (mood === 'sleepy' && tirednessLevel > 8 ) { //&& 조건
  console.log('time to sleep');
} else {
  console.log('not bed time yet');
}

//Truthy and Falsy
let wordCount = 0;
wordCount = 1; //1로만들어서 true 타게하기
if (wordCount) {
  console.log("Great! You've started your work!");
} else {
  console.log('Better get to work!');
}

let favoritePhrase = 'Hello World!';
favoritePhrase = ''; //없는값으로만들어 false 타게하기
if (favoritePhrase) {
  console.log("This string doesn't seem to be empty.");
} else {
  console.log('This string is definitely empty.');
}

//Truthy and Falsy Assignment
let tool = '';
let writingUtensil = tool || 'pen' //tool이 false라 writngUtensil에 pen
console.log(`The ${writingUtensil} is mightier than the sword.`);

let tool = '';
tool = 'marker'; //중간에 tool에 스트링을 넣었을때 출력의 변화
let writingUtensil = tool || 'pen' //tool이 true라 writngUtensil에 marker
console.log(`The ${writingUtensil} is mightier than the sword.`);

//Ternary Operator
let isLocked = false;
if (isLocked) {
  console.log('You will need a key to open the door.');
} else {
  console.log('You will not need a key to open the door.');
}
isLocked ? console.log('You will need a key to open the door.') : console.log('You will not need a key to open the door.');  //Ternary Operator 타입

let isCorrect = true;
if (isCorrect) {
  console.log('Correct!');
} else {
  console.log('Incorrect!');
}
isCorrect ? console.log('Correct!') : console.log('Incorrect!'); //Ternary Operator 타입

let favoritePhrase = 'Love That!';
if (favoritePhrase === 'Love That!') {
  console.log('I love that!');
} else {
  console.log("I don't love that!");
}
favoritePhrase === 'Love That!' ? console.log('I love that!') : console.log("I don't love that!"); //Ternary Operator 타입

//Else If Statements
let season = 'summer'; //season 에 else if 구문사용 winter,fall,summer 추가
if (season === 'spring') {
  console.log('It\'s spring! The trees are budding!');
} else if (season === 'winter') {
  console.log('It\'s winter! Everything is covered in snow.');
} else if (season === 'fall'){
  console.log('It\'s fall! Leaves are falling!');
} else if (season === 'summer'){
  console.log('It\'s sunny and warm because it\'s summer!');
} else {
}

//The switch keyword
let athleteFinalPosition = 'first place';
switch (athleteFinalPosition) { //각각의 case 와 default 일때 출력처리
  case 'first place':
  console.log('You get the gold medal!');
  break;

  case 'second place':
  console.log('You get the silver medal!');
  break;

  case 'third place':
  console.log('You get the bronze medal!');
  break;

  default:
  console.log('No medal awarded.');
  break;
}








