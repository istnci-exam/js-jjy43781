// ***********************************************
// Loops
// ***********************************************

//Repeating Tasks Manually
const vacationSpots = ['A', 'B', 'C'];
console.log(vacationSpots[0]);
console.log(vacationSpots[1]);
console.log(vacationSpots[2]);

//The For Loop
for (let counter = 5; counter < 11; counter++) {
    console.log(counter);
  } //output 5,6,7,8,9,10

//Looping in Reverse
for (let counter = 3; counter >= 0; counter--){
    console.log(counter);
  } //output 3,2,1,0

 //Looping through Arrays
 const vacationSpots = ['Bali', 'Paris', 'Tulum'];
 for (let i = 0; i < vacationSpots.length; i++){
   console.log('I would love to visit ' + vacationSpots[i]);
 }

//Nested Loops
const bobsFollowers = ['A', 'B', 'C', 'D'];
const tinasFollowers = ['B', 'C', 'E'];
const mutualFollowers = [];

for (let i = 0; i < bobsFollowers.length; i++) {
  for (let j = 0; j < tinasFollowers.length; j++) {
    if (bobsFollowers[i] === tinasFollowers[j]) {
      mutualFollowers.push(tinasFollowers[j]);
    }
  }
};

//The While Loop
const cards = ['diamond', 'spade', 'heart', 'club'];
let currentCard;
while (currentCard != 'spade') { //스페이드가 아니면 계속돌면서
  currentCard = cards[Math.floor(Math.random() * 4)]; //cards에 랜덤숫자를먹여
  console.log(currentCard);
};

//Do...While Statements
let cupsOfSugarNeeded = 9;
let cupsAdded = 6;
do {
cupsAdded++;
console.log(cupsAdded);
} while (cupsAdded < cupsOfSugarNeeded);

//The break Keyword
const rapperArray = ["Lil' Kim", "Jay-Z", "Notorious B.I.G.", "Tupac"];
for (let i = 0; i < rapperArray.length; i++) {
  console.log(rapperArray[i]);
  if (rapperArray[i] === 'Notorious B.I.G.') {
    break;
  } 
}
console.log("And if you don't know, now you know.")
















