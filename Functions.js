// ***********************************************
// Functions
// ***********************************************

//Function Declarations
getReminder(); //사용
function getReminder() {
    console.log('Water the plants.');
  } // 선언
  
greetInSpanish(); //사용
function greetInSpanish() {
  console.log('Buenas Tardes.');
} //선언

//Calling a Function
sayThanks();  //호출먼저하고 펑션선언해도된다
function sayThanks() {
  console.log('Thank you for your purchase! We appreciate your business.');
}

function sayThanks() {
    console.log('Thank you for your purchase! We appreciate your business.');
  }
  sayThanks(); //펑션선언후 호출
  sayThanks(); //여러번 호출 가능
  sayThanks();

//Parameters and Arguments
function sayThanks(name) {
    console.log('Thank you for your purchase '+ name + '! We appreciate your business.');
  }  
sayThanks('Cole');

//Default Parameters
function makeShoppingList(item1 = 'milk', item2 = 'bread', item3 = 'eggs') {
    console.log(`Remember to buy ${item1}`);
    console.log(`Remember to buy ${item2}`);
    console.log(`Remember to buy ${item3}`);
  }
  
  //Return
  function monitorCount(rows, columns){ //펑션선언
    return rows * columns; //계산 값 리턴
  }
  const numOfMonitors = monitorCount(5,4); //펑션호출하여 계산된값 변수에 지정
  console.log(numOfMonitors); //변수호출

  //Helper Functions
  function monitorCount(rows, columns) {
    return rows * columns;
  }
  function costOfMonitors(rows, columns) {
    return monitorCount(rows, columns) * 200;
  }
  const totalCost = costOfMonitors(5,4); //costOfMonitors를통해 MonitorCount값을 계산받아 처리
  console.log(totalCost); //출력

  //Function Expressions
  const plantNeedsWater = function plantNeedsWater(day) { if(day === 'Wednesday') {
    return true;
  } else {
    return false;
  }
  };
  console.log(plantNeedsWater('Tuesday')); //plantNeedsWater에 Tuesday 넣어서 펑션값으로 false 받고 출력

  //Arrow Functions
  const plantNeedsWater = (day) => {
    if (day === 'Wednesday') {
      return true;
    } else {
      return false;
    }
  };

  //Concise Body Arrow Functions
  const plantNeedsWater = day => day === 'Wednesday' ? true : false; // plantNeedsWater는 day가 수요일인지 아닌지에따라 true false를 받는다
  

  



