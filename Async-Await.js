// ***********************************************
// Async-Await
// ***********************************************

//The async Keyword
async function withAsync(num){
    if (num === 0){
        return 'zero';
      } else {
        return 'not zero';
      }
  }
  
  withAsync(100)
    .then((resolveValue) => {
    console.log(` withAsync(100) returned a promise which resolved to: ${resolveValue}.`);
  })

  //The await Operator
function nativePromiseDinner() {
    brainstormDinner().then((meal) => {
        console.log(`I'm going to make ${meal} for dinner.`);
    });
  }
  
    async function announceDinner() {
    // Write your code below:
    let meal = await brainstormDinner();
    console.log(`I'm going to make ${meal} for dinner.`); 
  }
  
  announceDinner();

  //Writing async Functions
  const shopForBeans = require('./library.js');

async function getBeans() {
  console.log(`1. Heading to the store to buy beans...`);
  let value = await shopForBeans();
  console.log(`3. Great! I'm making ${value} beans for dinner tonight!`);
}

getBeans();

//Handling Dependent Promises
const makeBeans = async() => {
    const type = await shopForBeans();
    let isSoft = await soakTheBeans(type);
    let dinner = await cookTheBeans(isSoft);
    console.log(dinner);
    };
    
    makeBeans();

//Handling Errors
const cookBeanSouffle = require('./library.js');

async function hostDinnerParty() {
  try {
   const souffle = await cookBeanSouffle();
    console.log(`${souffle} is served!`);
  }catch(error){
    console.log(error);
    console.log('Ordering a pizza!');
  }
}
hostDinnerParty();

//Handling Independent Promises
let {cookBeans, steamBroccoli, cookRice, bakeChicken} = require('./library.js');
async function serveDinner() {
 const vegetablePromise = steamBroccoli();
 const starchPromise = cookRice();
 const proteinPromise = bakeChicken();
 const sidePromise = cookBeans();

 console.log(`Dinner is served. We're having ${await vegetablePromise}, ${await starchPromise}, ${await proteinPromise}, and ${await sidePromise}.`);
};

serveDinner();

//Await Promise.all()
let {cookBeans, steamBroccoli, cookRice, bakeChicken} = require('./library.js');

// Write your code below:
async function serveDinnerAgain() {
  let foodArray = await Promise.all([steamBroccoli(), cookRice(), bakeChicken(), cookBeans()]);
  let vegetable = foodArray[0];
  let starch =  foodArray[1];
  let protein =  foodArray[2];
  let side =  foodArray[3];
  
  console.log(`Dinner is served. We're having ${vegetable}, ${starch}, ${protein}, and ${side}.`);
}
serveDinnerAgain();











