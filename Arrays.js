// ***********************************************
// Arrays
// ***********************************************

//Create an Array
const hobbies = ['1', '2', '3'];
console.log(hobbies);

//Accessing Elements
const famousSayings = ['Fortune favors the brave.', 'A joke is a very serious thing.', 'Where there is love there is life.'];
const listItem = famousSayings[0]; //배열중 첫번째만 기록
console.log(listItem);
console.log(famousSayings[2]);
console.log(famousSayings[3]); //undefined

//Update Elements
let groceryList = ['bread', 'tomatoes', 'milk'];
groceryList[1] = 'avocados'; //2번째 엘리먼트 값 바꿔넣기

//Arrays with let and const
let condiments = ['Ketchup', 'Mustard', 'Soy Sauce', 'Sriracha'];
const utensils = ['Fork', 'Knife', 'Chopsticks', 'Spork'];

condiments = ['Mayo'];
console.log(condiments);

utensils[3] = 'Spoon';
console.log(utensils);

//The .length property
const objectives = ['Learn a new languages', 'Read 52 books', 'Run a marathon'];
console.log(objectives.length); //배열몇개측정

//The .push() Method
const chores = ['wash dishes', 'do laundry', 'take out trash'];

chores.push('1', '2'); //배열추가
console.log(chores);

//The .pop() Method
const chores = ['wash dishes', 'do laundry', 'take out trash', 'cook dinner', 'mop floor'];
chores.pop(); //배열의 마지막 빼기
console.log(chores);

//More Array Methods
const groceryList = ['orange juice', 'bananas', 'coffee beans', 'brown rice', 'pasta', 'coconut oil', 'plantains'];

groceryList.shift();
console.log(groceryList);

groceryList.unshift('popcorn'); //팝콘추가
console.log(groceryList);

console.log(groceryList.slice(1, 4)); //?? 1~3까진데.. 모르겠음
console.log(groceryList);

const pastaIndex = groceryList.indexOf('pasta');  //배열중 파스타의번호를 저장
console.log(pastaIndex);

//Arrays and Functions
const concept = ['arrays', 'can', 'be', 'mutated'];

function changeArr(arr){
  arr[3] = 'MUTATED';
}

changeArr(concept); //배열3의 내용 변경
console.log(concept);

function removeElement(newArr){
  newArr.pop();
}

removeElement(concept); //배열의 마지막 내용 제거
console.log(concept);

//Nested Arrays
const numberClusters = [[1,2],[3,4],[5,6]]; //중첩 배열
const target = numberClusters[2][1]; //output 6





