// ***********************************************
// Introduction to javaScript
// ***********************************************

//Console
console.log(31); //나이
console.log(2); //JS 몇주차

//Comments
// Opening line.
/* console.log('The first time Yossarian saw the chaplain he fell madly in love with him.');
console.log('Yossarian was in the hospital with a pain in his liver that fell just short of being jaundice.');
console.log('The doctors were puzzled by the fact that it wasn\'t quite jaundice.');
console.log('If it became jaundice they could treat it.');
console.log('If it didn\'t become jaundice and went away they could discharge him.');
console.log('But this just being short of jaundice all the time confused them.'); */

//Data Types
console.log('JavaScript');
console.log(2011);
console.log('Woohoo! I love to code! #codecademy');
console.log(20.49);

//Arithmetic Operators
console.log(31 + 3.5); // +연산
console.log(2021 - 1969); // -연산
console.log(65 / 240); // /연산
console.log(0.2708 * 100); // *연산

//String Concatenation
console.log('Hello' + 'World'); //스트링 결합
console.log('Hello' + ' '+ 'World'); //스트링 결합

//Properties
console.log('Teaching the world how to code'.length); //길이측정

//Methods
console.log('Codecademy'.toUpperCase()); //대문자 처리
console.log('    Remove whitespace   '.trim()); //공백 제거

//Built-in Objects
console.log(Math.random()*100); //난수생성 * 100
console.log(Math.floor(Math.random()*100)); //(난수생성 *100) 정수처리
console.log(Math.ceil(Math.floor(Math.random()*100) >= 43.8)); // ((난수생성 *100) 정수처리) >= 43.8 비교
console.log(Number.isInteger(2017)); //2017이 정수형인지 확인하여 true / false 를 뱉어준다.

// ***********************************************
// Variables
// ***********************************************

//Create a Variable: var
var favoriteFood = 'pizza'; // 변수선언 및 문자열 연결
var numOfSlices = 8; //변수선언 및 숫자 연결
console.log(favoriteFood); //변수에 연결된 문자열 출력
console.log(numOfSlices); //변수에 연결된 숫자 출력

//Create a Variable: let
let changeMe = true; //변수선언 및 트루값 
changeMe = false; //변수의 연결값 변경
console.log(changeMe); //출력

//Create a Variable: const
const entree = 'Enchiladas'; //entree 에 값 넣기
console.log(entree); // 출력
entree = 'Tacos'; // 변수 재할당 에러

//Mathematical Assignment Operators
levelUp += 5; //levelUp = levelUp + 5;
powerLevel -= 100; // powerLevel = powerLevel - 100 ;
multiplyMe *= 11; // multiplyMe = multiplyMe * 11;
quarterMe /= 4; // quarterMe = quarterMe / 4 ;

//The Increment and Decrement Operator
gainedDollar++; // 1증가
lostDollar--; // 1감소

//String Concatenation with Variables
let favoriteAnimal = 'dog'; //문자열 변수결합
console.log('My favorite animal:' + favoriteAnimal ); //호출

//String Interpolation
const myName = 'JeongJaeYeong'; 
console.log(`My name is ${myName}.`); //
const myCity = 'Seoul';
console.log(`My favorite city is ${myCity}.`);
console.log(`My name is ${myName} My favorite city is ${myCity}.`); 

//typeof operator
let newVariable = 'Playing around with typeof.'; //변수 할당
console.log(typeof newVariable); //typeof 사용
newVariable = 1; //변수 재할당
console.log(typeof newVariable); //typeof 사용
newVariable = 'gggg'; ////변수 재할당
console.log(typeof newVariable); //typeof 사용








