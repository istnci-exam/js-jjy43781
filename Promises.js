// ***********************************************
// JavaScript Promises
// ***********************************************

//Constructing a Promise Object
const inventory = {
    sunglasses: 1900,
    pants: 1088,
    bags: 1344
  };
  
  // Write your code below:
  const myExecutor = (resolve, reject) => {
    if (inventory.sunglasses > 0) {
      resolve('Sunglasses order processed.');
    } else {
      reject('That item is sold out.');
    }
  };
  
  const orderSunglasses = () => {
    return new Promise(myExecutor);
  };
  
  const orderPromise = orderSunglasses();
  console.log(orderPromise);
  const inventory = {
    sunglasses: 1900,
    pants: 1088,
    bags: 1344
  };
  
  // Write your code below:
  const myExecutor = (resolve, reject) => {
    if (inventory.sunglasses > 0) {
      resolve('Sunglasses order processed.');
    } else {
      reject('That item is sold out.');
    }
  };
  const orderSunglasses = () => {
    return new Promise(myExecutor);
  };
  const orderPromise = orderSunglasses();
  console.log(orderPromise);

//The Node setTimeout() Function
console.log("This is the first line of code in app.js.");
const usingSTO = () => {
  console.log("This is the first line of code in app.js.");
};
setTimeout(usingSTO, 2000);
console.log("This is the last line of code in app.js.");

//Success and Failure Callback Functions
const handleSuccess = (resolvedValue) => {
  console.log(resolvedValue);
};

const handleFailure = (rejectionReason) => {
  console.log(rejectionReason);
}

const checkInventory = (order) => {
  return new Promise((resolve, reject) =>{
  }

checkInventory(order).then(handleSuccess, handleFailure);

//Using catch() with Promises
checkInventory(order).then(handleSuccess)
  .catch(handleFailure);

//Chaining Multiple Promises
const {checkInventory, processPayment, shipOrder} = require('./library.js');

const order = {
  items: [['sunglasses', 1], ['bags', 2]],
  giftcardBalance: 79.82
};

checkInventory(order)
.then((resolvedValueArray) => {
  // Write the correct return statement here:
 return processPayment(resolvedValueArray);
})
.then((resolvedValueArray) => {
  // Write the correct return statement here:
  return shipOrder(resolvedValueArray);
})
.then((successMessage) => {
  console.log(successMessage);
})
.catch((errorMessage) => {
  console.log(errorMessage);
});

//Avoiding Common Mistakes
checkInventory(order)
    .then((resolvedValueArray) => {
      return processPayment(resolvedValueArray);
      })
            .then((resolvedValueArray) => {
              return shipOrder(resolvedValueArray);
            })
                    .then((successMessage) => {
                      console.log(successMessage);
                    });

//Using Promise.all()
const checkSunglasses = checkAvailability('sunglasses', 'Favorite Supply Co.');
const checkPants = checkAvailability('pants', 'Favorite Supply Co.'); 
const checkBags = checkAvailability('bags', 'Favorite Supply Co.');

Promise.all([checkSunglasses, checkPants, checkBags]).then(onFulfill).catch(onReject);



